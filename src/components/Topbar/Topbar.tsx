import * as React from 'react';
import "./Topbar.css";

export const Topbar: React.FC = () => {
  return (
    <div className="topbar">
      <div className="app-name">Notes App</div>
    </div>
  );
}