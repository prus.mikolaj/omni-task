import React from 'react';
import './App.css';
import { Topbar } from './components/Topbar/Topbar';
import { Dashboard } from './components/Dashboard/Dashboard';
import { Provider } from 'react-redux';
import store from './store';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect
} from "react-router-dom";
import { SingleNote } from './components/SingleNote/SingleNote';

function App() {
  return (
    <Provider store={store}>
      <Router>
        <div className="App">
          <Topbar />
          <Switch>
            <Route exact path="/notes" component={Dashboard} />
            <Route exact path="/singlenote" component={SingleNote} />
            <Route exact path="/" render={() => (
              <Redirect to="/notes"/>
            )}/>
          </Switch>
        </div>
      </Router>
    </Provider>
  );
}

export default App;
