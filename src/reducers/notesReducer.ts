import {
  ADD_NOTE,
  DELETE_NOTE,
  SET_CURRENT_NOTE
} from "../actions/notesActions";

export interface INotesState {
  notes: INote[],
  currentNote: INote | undefined
}

const firstNote = {
  id: 1,
  text: "first note",
  timestamp: new Date()
}

const initialState: INotesState = {
  notes: [firstNote],
  currentNote: firstNote
}

export function notesReducer(state: INotesState = initialState, action: any): INotesState {
  switch (action.type) {
    case ADD_NOTE:
      const updatedNotes = [...state.notes, {
        id: Math.random(),
        text: action.text,
        timestamp: new Date()
      }];
      return {
        ...state,
        notes: updatedNotes
      };
    case DELETE_NOTE:
      const notes = state.notes.filter(note => note.id !== action.id);
      return {
        ...state,
        notes: notes
      };
    case SET_CURRENT_NOTE:
      const i = state.notes.findIndex(note => note.id === action.id);
      return {
        ...state,
        currentNote: state.notes[i]
      };
    default:
      return state;
  }
}