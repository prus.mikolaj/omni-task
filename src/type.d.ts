interface INote {
  id: number,
  text: string,
  timestamp: Date
}

type ContextType = {
  notes: INote[],
  saveNote: (text: string) => void,
  deleteNote: (id: number) => void
}
