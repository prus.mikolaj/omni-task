export const ADD_NOTE = "ADD_NOTE";
export const DELETE_NOTE = "DELETE_NOTE";
export const SET_CURRENT_NOTE = "SET_CURRENT_NOTE";

export const addNote = (text: string) => {
  return {
    type: ADD_NOTE,
    text: text
  }
}

export const deleteNote = (id: number) => {
  return {
    type: DELETE_NOTE,
    id: id
  }
}

export const setCurrentNote = (id: number) => {
  return {
    type: SET_CURRENT_NOTE,
    id: id
  }
}