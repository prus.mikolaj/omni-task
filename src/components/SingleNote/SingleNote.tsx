import * as React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { IRootState } from '../../store';
import './SingleNote.css';
import ReactMarkdown from "react-markdown";
import { displayDate } from '../../utils/displayDate';
import { useHistory } from "react-router-dom";
import { deleteNote } from '../../actions/notesActions';

export const SingleNote: React.FC = () => {
  const note = useSelector((state: IRootState) => state.notes.currentNote);
  const history = useHistory();
  const dispatch = useDispatch();

  const handleDelete = (id: number) => {
    dispatch(deleteNote(id));
    history.goBack();
  }

  const handleGoBack = () => {
    history.goBack();
  }
  
  return (
    note ?
    <div className="single-note">
      <div className="buttons-container">
        <button className="back-button" onClick={handleGoBack}>Go back</button>
        <button className="delete-button" onClick={() => handleDelete(note.id)}>Delete note</button>
      </div>
      <div className="note-container">
        <div className="note-body">
          <ReactMarkdown source={note.text} />
        </div>
        <div className="note-timestamp">
          {displayDate(note.timestamp)}
        </div>
      </div>
    </div>
    : <></>
  );
}