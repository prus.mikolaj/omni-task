import { combineReducers, createStore } from 'redux';
import { notesReducer, INotesState } from './reducers/notesReducer';
export interface IRootState {
    notes: INotesState
}
const store = createStore<IRootState, any, any, any>(
    combineReducers({
        notes: notesReducer
}));
export default store;