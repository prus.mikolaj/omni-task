import React, { ChangeEvent, useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { IRootState } from '../../../store';
import { addNote } from '../../../actions/notesActions';
import "./NoteInput.css";

export const NoteInput: React.FC = () => {
  const [value, setValue] = useState("");
  const notes = useSelector((state: IRootState) => state.notes.notes);
  const handleChange = (e: ChangeEvent<HTMLTextAreaElement>) => setValue(e.target.value);
  const dispatch = useDispatch();

  const handleSubmit = (e: React.FormEvent) => {
    e.preventDefault();
    dispatch(addNote(value));
    setValue("");
  }

  return (
    <>
    <h3>Note</h3>
    <form onSubmit={(e) => handleSubmit(e)}>
      <textarea className="note-textarea" rows={15} cols={100} value={value} onChange={handleChange}/>
      <button className="add-note-button" disabled={value.length < 1} type="submit">Add Note</button>
    </form>
    </>
  );
}
