import React from 'react';
import { useDispatch } from 'react-redux';
import { deleteNote, setCurrentNote } from '../../../actions/notesActions';
import './Note.css';
import ReactMarkdown from "react-markdown";
import { displayDate } from '../../../utils/displayDate';
import { useHistory } from "react-router-dom";

interface Props {
  note: INote
};

export const Note: React.FC<Props> = ({
  note
}) => {
  const dispatch = useDispatch();
  let history = useHistory();

  const handleDelete = (id: number) => {
    dispatch(deleteNote(id));
  }

  const handleTimestampClick = () => {
    dispatch(setCurrentNote(note.id));
    history.push("/singlenote");
  }
  
  return (
    <div className="note">
      <div className="note-body">
        <div className="note-text">
          <ReactMarkdown source={note.text} />
        </div>
        <div className="timestamp">
          <span onClick={handleTimestampClick}>{displayDate(note.timestamp)}</span>
        </div>
      </div>
      <div className="note-button-container">
        <button className="delete-button" onClick={() => handleDelete(note.id)}>Delete note</button>
      </div>
    </div>
  );
}
