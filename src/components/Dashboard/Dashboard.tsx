import * as React from 'react';
import { NoteInput } from './NoteInput/NoteInput';
import { useSelector } from 'react-redux';
import { IRootState } from '../../store';
import { Note } from './Note/Note';
import './Dashboard.css';

export const Dashboard: React.FC = () => {
  const notes = useSelector((state: IRootState) => state.notes.notes);

  const sortByNewest = (notes: INote[]) => {
    return notes.sort((a,b) => {
      return (b.timestamp.getTime() - a.timestamp.getTime());
    });
  }

  return (
    <div className="dashboard">
      <NoteInput />
      <h2>Latest notes</h2>
      {sortByNewest(notes).map(note => <Note note={note}/>)}
    </div>
  );
}